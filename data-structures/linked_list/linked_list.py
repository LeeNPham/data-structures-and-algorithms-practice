# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_list.
#
# Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_19.py.

class LinkedListNode():
    def __init__(self, value):
        self.value = None
        self.link = None

class LinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self._length = 0    # This will be altered later when we go into the specific object which has the list of nodes, that's why we have the 
                            # @property to update the objects length value

    # "read-only" length property, this will help later in referencing the length of the nodes list so we can adjust accordingly
    @property
    def length(self):  # This is invoked before every other function below starts as a property of the list
        return self._length

    def traverse(self, index):
        # If list empty
        # If idx out of bounds
        if index >= self._length:
            raise IndexError("index out of range")
        node = self.head
        i = 0
        while i < index:
            node = node.link
            i += 1
        return node

    def get(self, index):   #get returns the value in the list at the specified
                            #index, or raises an IndexError if the index is outside
                            #the bounds of the list
        if index < 0:
            index = self._length + index
        node = self.traverse(index)
        return node.value

    def remove(self, index):
        self._length -= 1
        if index == 0:
            value = self.head.value
            self.head = self.head.link
            return value
        before_node = self.traverse(index - 1)
        node_to_remove = before_node.link
        next_node = node_to_remove.link
        value = node_to_remove.value
        before_node.link = next_node
        if next_node is None:
            self.tail = before_node
        return value

    def insert(self, value, index=None):
        node = LinkedListNode(value)
        # If list empty
        if self.head is None:
            self.head = node
            self.tail = node
        # If idx out of bounds
        elif index is not None and index < self._length:
            # If idx 0
            if index == 0:
                node.link = self.head
                self.head = node
            # If a node is big enough to accommodate an insertion
            else:
                old_node = self.traverse(index - 1) #find the position of the node you wish to insert after(Thats why it traverses by index -1)
                node.link = old_node.link #replace the new nodes link with the old nodes link
                old_node.link = node #replaceing the old nodes link with a link to the new node that was inserted
        else: #If the node list is empty
            self.tail.link = node
            self.tail = node
        self._length += 1 #updating length of linked list to accommodate inserted node




