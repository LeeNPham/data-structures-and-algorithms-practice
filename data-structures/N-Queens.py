from pprint import pprint

board = [
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
]

def is_safe(board, row, column):
    # Check row & column
    for i in range(column):
        if board[row][i] == 1:
            return False

    # Check upper diagonal on left side
    for i, j in zip(range(row, -1, -1), range(column, -1, -1)):
        if board[i][j] == 1:
            return False

    # Check lower diagonal on left side
    for i, j in zip(range(row, len(board[0]), 1), range(column, -1, -1)):
        if board[i][j] == 1:
            return False            
    
    return True

def place_queen(board, column_idx):
    if column_idx >= (len(board[0])):
        return True
    for row_idx in range(len(board[0])):
        if is_safe(board, row_idx, column_idx):
            board[row_idx][column_idx] = 1
            if place_queen(board, column_idx+1):
                return True
            else:
                board[row_idx][column_idx] = 0
    return False  # NO solution possible

def eight_queens():
    board = [
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
    ]

    place_queen(board, 0)
    return board # solved board

if __name__ == "__main__":
    board = eight_queens()
    pprint(board, width=100)