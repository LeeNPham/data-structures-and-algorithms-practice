extra_memory = {}


def max_cut_stock(pipe_length, values):
    max_value = values.get(pipe_length, 0)

    if pipe_length in extra_memory:
        return extra_memory[pipe_length]

    for cut_length in range(1, pipe_length):
        cut_value = values.get(cut_length, 0)
        remaining_length = pipe_length - cut_length
        max_remaining_value = max_cut_stock(remaining_length, values)
        if cut_value + max_remaining_value > max_value:
            max_value = cut_value + max_remaining_value

    extra_memory[pipe_length] = max_value

    return max_value


n = 100
values = {
    1: 10,
    2: 25,
    3: 27,
    4: 50,
    5: 38,
}

print("Max pipe value:", max_cut_stock(n, values))
