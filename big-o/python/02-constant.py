"""
| Class       | f(x)       | Big O       | You can find it in things that...                      |
|-------------|------------|-------------|--------------------------------------------------------|
| Constant    | c          | O(c)        | contain only simple statements                         |
"""

def constant(n):
    return 2 * n + n ^ n


if __name__ == "__main__":
    n = 10.2
    print(constant(n))
