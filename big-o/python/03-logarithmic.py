"""
| Class       | f(x)       | Big O       | You can find it in things that...                      |
|-------------|------------|-------------|--------------------------------------------------------|
| Logarithmic | log(n)     | O(log n)    | cut things in half over and over                       |
"""

def binary_search(values, value_to_find):
    start = 0
    end = len(values) - 1
    while start <= end:
        middle = (start + end) // 2
        if values[middle] == value_to_find:
            return middle
        elif values[middle] < value_to_find:
            start = middle + 1
        else:
            end = middle - 1
    return None


if __name__ == "__main__":
    values = list(range(1_234_567)) + [23_842_903_842]
    value_to_find = 23_842_903_842
    print(binary_search(values, value_to_find))
