"""
| Class       | f(x)       | Big O       | You can find it in things that...                      |
|-------------|------------|-------------|--------------------------------------------------------|
| Exponential | k^n        | O(k^n)      | usually find subsets of a main set                     |
"""

def fibonacci(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    return fibonacci(n - 1) + fibonacci(n - 2)


if __name__ == "__main__":
    print(fibonacci(20))
