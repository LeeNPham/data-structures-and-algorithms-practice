"""
| Class       | f(x)       | Big O       | You can find it in things that...                      |
|-------------|------------|-------------|--------------------------------------------------------|
| Superlinear | n * log(n) | O (n log n) | usually cut things in half and contains a loop         |
"""

def merge_sort(values):
    if len(values) <= 1:
        return values
    middle = len(values) // 2
    left = values[0:middle]
    right = values[middle:]
    return merge(merge_sort(left), merge_sort(right));


def merge(left, right):
    result = []
    leftIndex = 0
    rightIndex = 0
    while (
        leftIndex < len(left) and
        rightIndex < len(right)
    ):
        if left[leftIndex] < right[rightIndex]:
            result.append(left[leftIndex])
            leftIndex += 1
        else:
            result.append(right[rightIndex])
            rightIndex += 1
    if leftIndex < len(left):
        result += left[leftIndex:]
    elif rightIndex < len(right):
        result += right[rightIndex:]
    return result


if __name__ == "__main__":
    from random import shuffle
    values = list(range(20))
    shuffle(values)
    print("Sorting", values)
    print(merge_sort(values))
