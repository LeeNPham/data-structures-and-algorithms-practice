"""
| Class       | f(x)       | Big O       | You can find it in things that...                      |
|-------------|------------|-------------|--------------------------------------------------------|
| Linear      | n          | O(n)        | usually contains a single for loop based on input size |
"""

def sum_list(values):
    sum = 0
    for value in values:
        sum += value
    return sum


if __name__ == "__main__":
    values = list(range(1000))
    print(sum_list(values))
