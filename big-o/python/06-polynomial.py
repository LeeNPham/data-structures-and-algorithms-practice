"""
| Class       | f(x)       | Big O       | You can find it in things that...                      |
|-------------|------------|-------------|--------------------------------------------------------|
| Polynomial  | n^k        | O(n^k)      | usually has k nested loops                             |
"""

def create_2d_array(rows, cols):
    result = []
    for i in range(rows):
        row = []
        for j in range(cols):
            row.append(0)
        result.append(row)
    return result;


if __name__ == "__main__":
    from pprint import pprint
    pprint(create_2d_array(3, 15))
