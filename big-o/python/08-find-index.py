"""
search: "abc"
text:   "aabbcaabacabcaa"
"""

def find_index(search, text):
    m = len(search);
    n = len(text);

    for i in range(n - m + 1):
        j = 0;
        while j < m and text[i + j] == search[j]:
            j += 1
        if j == m:
            return i;
    return -1;


if __name__ == "__main__":
    search = "abc"
    text = "aabbcaabacabcaa"
    print(find_index(search, text))
