/*
| Class       | f(x)       | Big O       | You can find it in things that...                      |
|-------------|------------|-------------|--------------------------------------------------------|
| Superlinear | n * log(n) | O (n log n) | usually cut things in half and contains a loop         |
*/

function mergeSort(unsorted) {
  if (unsorted.length <= 1)  return unsorted;

  const middle = Math.floor(unsorted.length / 2);

  const left = sorted.slice(0, middle);
  const right = sorted.slice(middle);

  return merge(mergeSort(left), mergeSort(right));
}

function merge(left, right) {
  let result = [], leftIndex = 0, rightIndex = 0;

  while (leftIndex < left.length && rightIndex < right.length) {
    if (left[leftIndex] < right[rightIndex]) {
      result.push(left[leftIndex]);
      leftIndex += 1;
    } else {
      result.push(right[rightIndex]);
      rightIndex += 1;
    }
  }

  return result;
}
