/*
| Class       | f(x)       | Big O       | You can find it in things that...                      |
|-------------|------------|-------------|--------------------------------------------------------|
| Linear      | n          | O(n)        | usually contains a single for loop based on input size |
*/

function sumArray(arr) {
  let sum = 0;
  for (let value of arr) {
    sum += value;
  }
  return sum;
}
