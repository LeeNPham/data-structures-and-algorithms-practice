/*
| Class       | f(x)       | Big O       | You can find it in things that...                      |
|-------------|------------|-------------|--------------------------------------------------------|
| Logarithmic | log(n)     | O(log n)    | cut things in half over and over                       |
*/

function binarySearch(arr, valueToFind) {
  let start = 0;
  let end = arr.length - 1;

  while (start <= end) {
    let middle = Math.floor((start + end) / 2);

    if (arr[middle] === valueToFind) {
      return middle;
    } else if (arr[middle] < valueToFind) {
      start = middle + 1;
    } else {
      end = middle - 1;
    }
  }

  return undefined;
}
