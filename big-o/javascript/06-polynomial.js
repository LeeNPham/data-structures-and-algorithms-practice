/*
| Class       | f(x)       | Big O       | You can find it in things that...                      |
|-------------|------------|-------------|--------------------------------------------------------|
| Polynomial  | n^k        | O(n^k)      | usually has k nested loops                             |
*/

function createTwoDimensionalArray(rows, cols) {
  const result = [];

  for (let i = 0; i < rows; i += 1) {
    const row = [];
    for (let j = 0; j < cols; j += 1) {
      row.push(0);
    }
    result.push(row);
  }

  return result;
}
