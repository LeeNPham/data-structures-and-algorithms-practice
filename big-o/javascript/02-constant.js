/*
| Class       | f(x)       | Big O       | You can find it in things that...                      |
|-------------|------------|-------------|--------------------------------------------------------|
| Constant    | c          | O(c)        | contain only simple statements                         |
*/

function constant(n) {
  return 2 * n + n ^ n;
}
