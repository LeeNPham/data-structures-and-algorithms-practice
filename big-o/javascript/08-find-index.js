/*
search: "abc"
text:   "aabbcaabacabcaa"
*/

function findIndex(search, text) {
  const m = search.length;
  const n = text.length;

  for (let i = 0; i <= n - m; i += 1) {
    let j = 0;
    while (j < m && text[i + j] === search[j]) {
      j += 1;
    }
    if (j === m) {
      return i;
    }
  }
  
  return -1;
}
