| Class       | f(x)       | Big O       | You can find it in things that...                      |
|-------------|------------|-------------|--------------------------------------------------------|
| Constant    | c          | O(c)        | contain only simple statements                         |
| Logarithmic | log(n)     | O(log n)    | cut things in half over and over                       |
| Linear      | n          | O(n)        | usually contains a single for loop based on input size |
| Superlinear | n * log(n) | O (n log n) | usually cut things in half and contains a loop         |
| Polynomial  | n^k        | O(n^k)      | usually has k nested loops                             |
| Exponential | k^n        | O(k^n)      | usually find subsets of a main set                     |