def sweep(items):
    swapped = False
    for i in range(0, len(items)-1):
        if items[i] > items[i+1]:
            items[i+1], items[i] = items[i], items[i+1]
            swapped = True
        if not swapped:
            break



def bubble_sort(items):
    for i in range(0, len(items)-1):
        #sweep(items)
        for idx in range(0, len(items)-i):
            if items[idx] > items[idx+1]:
                items[i+1], items[i] = items[i], items[i+1]

# Myabe you use "for in" loops when referencing the index

l = [5, 1, 8, 3, 7]
print("starting: ", l)
bubble_sort(l)
print("sorted: ", l)