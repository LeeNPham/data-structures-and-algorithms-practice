function partition(values, left, right) {
    let pivot = values[right]
    let star = left - 1
        for (let i= left; i < right; i++){
            if (values[i] <= pivot){
                star += 1
                var temp = values[star]
                values[star] = values[i]
                values[i] = temp
            }
        }
    star += 1
    var temp = values[star]
    values[star] = values[right]
    values[right] = temp
    return star
}


function quicksort(values, left=null, right=null) {
    if (left == null && right == null){
        left = 0
        right = values.length - 1
    }
    if (left >= right || left < 0){
        // return values
        return values
    }
    let p = partition(values, left, right)
    quicksort(values, left, p - 1)
    quicksort(values, p + 1, right)
}