function mergeSort(values, left=null, right=null) {
    if (left === null && right === null) {
        left = 0;
        right = (values.length) - 1;
    }
    if (left >= right) {
        return values;
    }
    
    let middle = Math.floor((right + left) / 2)
    mergeSort(values, left, middle)
    mergeSort(values, middle + 1, right)
    merge(values, left, middle, right)
}

function merge(values, left, middle, right) {
    let rightStart = middle + 1
    if (values[middle] <= values[rightStart]) {
        return;
    }
    while (left <= middle && rightStart <= right) {
        if (values[left] <= values[rightStart]){
            left += 1
        } else {
            let value = values[rightStart]
            let index = rightStart
            
            while (index !== left) {
                values[index] = values[index-1]
                index -= 1
            }
            values[left] = value
            left += 1
            middle += 1
            rightStart += 1
            }
        }
    }