def linear_search(values, target):
    for i, value in enumerate(values):
        if target == value:
            return i
    return -1