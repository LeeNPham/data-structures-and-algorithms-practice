tax_input = int(input("How much did you get payed this year? $"))


rates = [.1, .12, .22, .24, .32, .35, .37]
bracket = [10275, 41775, 89075, 170050, 215950, 539900]


def taxes_rec(income, i):
    if i == 0:
        return income - (income*.10)
    else:
        this_bracket = income - bracket[i-1]
        after_tax = this_bracket - (this_bracket * rates[i])
        left_over = taxes_rec(bracket[i-1], i -1)
        return after_tax + left_over

def taxes(income):
    if income <= 10275:
        i = 0
    if 10275 < income <= 41775:
        i = 1
    if 41775 < income <= 89075:
        i = 2
    if 89075 < income <= 170050:
        i = 3
    if 170050 < income <= 215950:
        i = 4
    if 215950 < income <= 539900:
        i = 5
    if income >= 539901:
        i = 6
    return taxes_rec(income, i)
        
print(taxes(tax_input))