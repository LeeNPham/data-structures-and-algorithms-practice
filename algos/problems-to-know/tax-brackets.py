tax_input = int(input("How much did you get payed this year? $"))
rate_1 = .1
rate_2 = .12
rate_3 = .22
rate_4 = .24
rate_5 = .32
rate_6 = .35
rate_7 = .37
def taxes_1(income):
    return income - (income*.10)

def taxes_2(income):
    this_bracket = income - 10275
    after_tax = this_bracket - (this_bracket * rate_2)
    left_over = taxes_1(10275)
    return after_tax + left_over

def taxes_3(income):
    this_bracket = income - 41775
    after_tax = this_bracket - (this_bracket * rate_3)
    left_over = taxes_2(41775)
    return after_tax + left_over

def taxes_4(income):
    this_bracket = income - 89075
    after_tax = this_bracket - (this_bracket * rate_4)
    left_over = taxes_3(89075)
    return after_tax + left_over

def taxes_5(income):
    this_bracket = income - 170050
    after_tax = this_bracket - (this_bracket * rate_5)
    left_over = taxes_4(170050)
    return after_tax + left_over

def taxes_6(income):
    this_bracket = income - 215950
    after_tax = this_bracket - (this_bracket * rate_6)
    left_over = taxes_4(215950)
    return after_tax + left_over

def taxes_7(income):
    this_bracket = income - 539900
    after_tax = this_bracket - (this_bracket * rate_7)
    left_over = taxes_4(539900)
    return after_tax + left_over

def taxes(income):
    if income <= 10275:
        return taxes_1(income)
    if 10275 < income <= 41775:
        return taxes_2(income)
    if 41775 < income <= 89075:
        return taxes_3(income)
    if 89075 < income <= 170050:
        return taxes_4(income)
    if 170050 < income <= 215950:
        return taxes_5(income)
    if 215950 < income <= 539900:
        return taxes_6(income)
    if income >= 539901:
        return taxes_7(income)
        
print(taxes(tax_input))